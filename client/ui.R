# interface
source('server.R')


ui <- dashboardPage(
  dashboardHeader(title = "E-Réputation"),
  dashboardSidebar(
    
    # ajout d'un produit
    sidebarSearchForm(textId = "produit_entre", buttonId = "submit",label = "Ajouter un produit...",
                      icon = shiny::icon("plus")),
    # msg de confirmation
    hidden(div(id='rep',  
               div(style="margin-left:20px", textOutput("response"))
               
    )),
    
    selectInput(
      inputId = "produit_choisi",
      label = "Produit :",
      choices = product_list,
      selectize = FALSE
    ),
    div(style="margin-left:72px", actionBttn("chercher", "Valider", style="minimal", size="xs")),
    
    sidebarMenu(
    
    menuItem(" Youtube", tabName = "ytb", icon = icon("youtube")),
    menuItem(" Twitter", tabName = "twitter", icon = icon("twitter")),
    menuItem(" Amazon", tabName = "amazon", icon = icon("amazon")),
    menuItem("Comparaison", tabName = "dashboard", icon = icon("dashboard")),
    menuItem("Aide", tabName = "Aide", icon = icon("question-circle"))
  )),
  ## Body content
  dashboardBody(
    
    tags$head(tags$style("section.content { overflow-y: hidden; }")),
    
    tabItems(
      
      # Première page ########################################################
      tabItem(tabName = "dashboard",
              h1("Tableau de bord", align ="center"), br(),br(),
              fluidRow(
                box(status = "primary", solidHeader = TRUE,
                    title = "Filtre",selectizeInput("prod",
                                                    "choix de produit(s):",
                                                    choices = c("") ,
                                                    multiple = TRUE
                    )),
                box(status = "warning", solidHeader = TRUE,
                    title =  span(icon("amazon"),"Moyenne des notes"),
                    plotOutput("bar_2")%>% withSpinner(color="#0275D8") )
            
),
              fluidRow(
                box(status = "danger", solidHeader = TRUE,
                    title =  span(icon("youtube")," Evolution du nombre de vues"),
                    plotOutput("plot_1")%>% withSpinner(color="#0275D8") ),
                
                box(status = "primary", solidHeader = TRUE,
                    title =  span(icon("twitter")," Réactions sur Twitter"),
                    plotOutput("bar_1")%>% withSpinner(color="#0275D8") )
              )),

  
      # youtube ########################################################
      tabItem(tabName = "ytb",
              h3("YouTube", align="center"),
              box(solidHeader = TRUE, "Vous avez sélectionné :",textOutput("nom_selection"), width=12),
              fluidRow(
                valueBoxOutput("approvalBox"),
                valueBoxOutput("viewBox"),
                valueBoxOutput("sumText")
          )
                
              
      ),

     
      # twitter ########################################################
      
      tabItem(tabName = "twitter",
              h3("Twitter", align="center"),
             box(solidHeader = TRUE, "Vous avez sélectionné :",textOutput("nom_selection2"),width=12),
              
            fluidRow(
              box(
                title = "Nuage de mots positifs", status = "primary",
                plotOutput("wordcloudpostw")
              ),
              
              box(
                title = "Nuage de mots négatifs", status = "warning",
                plotOutput("wordcloudnegtw")
              )),
            
            fluidRow(
              tabBox(
                side = "right",
                selected = "Total",
                height = "250px",
                tabPanel("Tweets", "Total de tweets positifs", br(), br(), 
                         infoBoxOutput("txtposTwitter", width = "100%")),
                tabPanel("Favoris", "Total de favoris positifs", br(), br(), 
                         infoBoxOutput("likeposTwitter", width = "100%")),
                tabPanel("Retweets", "Total de retweets positifs", br(), br(), 
                         infoBoxOutput("rtposTwitter", width = "100%")),
                tabPanel("Total", "Nombre total de réaction positives concernant le produit : 
                         tweets, retweets et favoris.", br(), br(), 
                         infoBoxOutput("sumposTwitter", width = "100%"))
              ),
              tabBox(
                side = "right",
                selected = "Total",
                height = "250px",
                tabPanel("Tweets", "Total de tweets négatifs", br(), br(), 
                         infoBoxOutput("txtnegTwitter", width = "100%")),
                tabPanel("Favoris", "Total de favoris négatifs", br(), br(), 
                         infoBoxOutput("likenegTwitter", width = "100%")),
                tabPanel("Retweets", "Total de retweets négatifs", br(), br(), 
                         infoBoxOutput("rtnegTwitter", width = "100%")),
                tabPanel("Total", "Nombre total de réaction négatives concernant le produit : 
                         tweets, retweets et favoris.", br(), br(), 
                         infoBoxOutput("sumnegTwitter", width = "100%"))
              )
            ),
            fluidRow(
              box(status = "primary", solidHeader = TRUE,title = span(icon("twitter")," Twitte le plus positif"),
                  htmlOutput(outputId ="twitPos")
                  
              ),
              box(status = "primary", solidHeader = TRUE,title = span(icon("twitter")," Twitte le plus negatif"),
                  htmlOutput(outputId ="twitNeg")
                  
              ),
            )
            
            
            
            
            
              
      ),
      
      
      # amazon ########################################################
      
      tabItem(tabName = "amazon",
              h3("Amazon", align="center"),
              box(solidHeader = TRUE, "Vous avez sélectionné :",textOutput("nom_selection3"), width=12),
              
              fluidRow(
                infoBoxOutput("moynoteposBox"),
                infoBoxOutput("moynotenegBox")
                
                
              ),
              
              fluidRow(
                infoBoxOutput("composBox"),
                infoBoxOutput("comnegBox")
              ),
              fluidRow(
                box(status = "warning", solidHeader = TRUE,title = span(icon("amazon")," commentaire le plus positif"),
                    htmlOutput(outputId ="amaPos")
                    
                ),
                box(status = "warning", solidHeader = TRUE,title = span(icon("amazon")," commentaire le plus negatif"),
                    htmlOutput(outputId ="amaNeg")
                    
                ),
              )
      ),
# Manuel ########################################################

tabItem(tabName = "Aide",
        h2("Aide", align="center"),
        box(solidHeader = TRUE,  
            h4(" Bienvenu sur  E-réputation l'application dédier au suivie de la réputation de produits sur le net 
            (Twitter, Amazon et Youtube pour cette version ) dans ce qui suit nous vous expliquerons le mode 
            d'emplois de notre application:"), width = 12),
        box(solidHeader = TRUE, width = 12,
            h3("Choix de produit"),hr(),br(),
            h5("Afin de séléctioner un produit il vous suffit de cliquer sur la liste déroulante du menu de gauche puis de choisir 
            cliquer sur le produit désiré, ensuite il vous suffit decliquer sur valider "),
            br(),
            div(img(src = "select.PNG", width = 800), style="text-align: center;")
            
            ),
        box(solidHeader = TRUE, width = 12,
            h3("Ajout de produit"),hr(),br(),
            h5("Afin d'ajouter un nouveau produit il suffit de saisir son nom dans la barre de gauche (1), puis de cliquer sur ajout (+), au bout de 45s 
               à 1 minute 30 le produit serra chargé en base "),
            br(),
            div(img(src = "ajout produit.PNG", width = 800), style="text-align: center;")
            
        ),
        
        box(solidHeader = TRUE, width = 12,
            h3("Selectioner une categorie"),hr(),br(),
            h5(" Une fois un produit séléctioné il vous suffit de cliquer sur l'une des catégories du menu de gauche pour y suivre son detail "),
            br(),
            div(img(src = "categorie.PNG", width = 1200), style="text-align: center;")
            
        ),
        
        box(solidHeader = TRUE, width = 12,
            h3("Youtube"),hr(),br(),
            h5(" Dans cet onglet vous pourrez suivre le nombre de vue, de like et de commentaires cumulés par l'ensembles des videos traitant du produits  "),
            br(),
            div(img(src = "youtube.PNG", width = 1200), style="text-align: center;")
            
        ),        
        box(solidHeader = TRUE, width = 12,
                      h3("Twitter"),hr(),br(),
                      h5(" Dans cet onglet, vous pourrez suivre plusieurs parametre réparties en trois carégories:"),
                      br(),
                      h5("1 - deux nuages de mots fait à base des twitts positif et des twittes négatifs"),
                      h5("2 - permet de suivre le nombre cumulé de like, retweet du produit"),
                      h5("3 - affiches le twitte le postif le plus liké et celui négatif le plus liké"),
                      div(img(src = "twitter.PNG", width = 1200), style="text-align: center;")
                      
        ),
        box(solidHeader = TRUE, width = 12,
            h3("Amazon"),hr(),br(),
            h5(" Dans cet onglet, vous pourrez suivre plusieurs parametre propre à amazon comme le nombre de commentaires postifs, 
               le nombre de commentaires négatifs, la moyenne des notes pour les commentaires négatifs et celle des commentaires positifs"),
            br(),
            
            div(img(src = "amazon.PNG", width = 1200), style="text-align: center;")
            
        ),
        
        box(solidHeader = TRUE, width = 12,
            h3("Comparaison"),hr(),br(),
            h5(" Dans cet onglet, vous pourrez comparer l'évolution du nombre de twitts , vue sur youtube et note totale moyenne de plusieurs 
               produits sur plusieurs jours "),
            br(),
            div(img(src = "comparaison.PNG", width = 1200), style="text-align: center;")
            
        ),
        
        
        )


      
    

    )
  )
)
