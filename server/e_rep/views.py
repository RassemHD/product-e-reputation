from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.views import APIView
from rest_framework import status
from .models import Product, Features, GoodText
from .serializers import Product_Serializer, Features_Serializer, GoodText_Serializer
from scripts import Data_Loader
from django.forms.models import model_to_dict
import threading
import json

data_loader = Data_Loader()

def index(request):
    return render(request,'index.html')

class Product_List(APIView):

    def get(self,request,format=None):
        products = Product.objects.all()
        serializer = Product_Serializer(products,many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self,request,format=None):
        serializer = Product_Serializer(data = request.data)
        if(serializer.is_valid()):
            product = serializer.save()
            thread = threading.Thread(target=data_loader.start, args=(product,))
            thread.start()
            return JsonResponse(serializer.data,safe=False,status=status.HTTP_201_CREATED)
        else:
            return JsonResponse(serializer.errors,safe=False,status = status.HTTP_400_BAD_REQUEST)

class Product_Details(APIView):

    def get(self,request,pk,format=None):

        if(request.GET.get('target',False) =='text'):

            obj = get_object_or_404(GoodText.objects.all(),pk=pk)
            results = model_to_dict(obj)
            results["pos_word_cloud"] = json.loads(results["pos_word_cloud"])
            results["neg_word_cloud"] = json.loads(results["neg_word_cloud"])

        else:

            items = Features.objects.filter(product_id=pk)
            serializer = Features_Serializer(items,many=True)
            results = serializer.data
        
        return JsonResponse(results,safe=False,status=status.HTTP_200_OK)

    def delete(self, request, pk):
        product = get_object_or_404(Product.objects.all(),pk=pk)
        product.delete()
        return HttpResponse("product with id `{}` has been deleted.".format(pk),status=status.HTTP_200_OK)
