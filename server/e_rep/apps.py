from django.apps import AppConfig


class EReputationConfig(AppConfig):
    name = 'e_reputation'
