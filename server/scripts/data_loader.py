from e_rep.models import Product, Twitter, Youtube, Amazon, Scraping_Url
from .neural_networks import Simple_Pred
from .data_api.twitter_api import Twitter_API
from .data_api.youtube_api import Youtube_API
from .data_api.amazon_scraper import Amazon_Scraper
from .data_miner import save_features, save_text

class Data_Loader:

    def __init__(self):
        # Neural network
        predictor = Simple_Pred('scripts/neural_networks/Simple_RNN/serial/model.h5','scripts/neural_networks/Simple_RNN/serial/tokenizer.pickle')
        # Data APIs
        self.twitter = Twitter_API(predictor,"scripts/data_api/twitter_api/twitter_credentials.json")
        self.youtube = Youtube_API(predictor,"scripts/data_api/youtube_api/youtube_credentials.txt")
        self.amazon = Amazon_Scraper(predictor,"scripts/data_api/amazon_scraper/selectors.yml")

    # get data from all available sources for a given product   
    def load(self,product):

        print("Amazon >>> %s ..." % product.name)
        self.amazon.get_data(product)

        print("Twitter >>> %s ..." % product.name)
        self.twitter.get_data(product)

        print("Youtube >>> %s ..." % product.name)
        self.youtube.get_data(product)
        
    # remove all data for a given product 
    def flush(self,product):
        Twitter.objects.filter(product=product.id).delete()
        Youtube.objects.filter(product=product.id).delete()
        Amazon.objects.filter(product=product.id).delete()

    # load data -> process -> clear raw data, for a given product
    def start(self,product):
        self.load(product)
        save_features(product)
        save_text(product)
        self.flush(product)
        print("[%s] ... done!" % product.name)

    # update data for multiple products
    def process_many(self,products):
        for product in products:
            self.start(product)
        print("OK")

#------------------#
# update data base #
#------------------#
def run():

    data_loader = Data_Loader()
    products = Product.objects.all()
    data_loader.process_many(product)