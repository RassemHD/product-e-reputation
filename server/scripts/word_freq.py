import nltk
#nltk.download('stopwords')
#nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
import string

class Word_Freq:

	def __init__(self,lang='english'):
		self.stop_words = set(stopwords.words(lang))
		self.stemmer = SnowballStemmer(lang)

	def clean_words(self,text):
		# split into words
		tokens = word_tokenize(text)
		# convert to lower case
		tokens = [w.lower() for w in tokens]
		# remove punctuation from each word
		table = str.maketrans('', '', string.punctuation)
		stripped = [w.translate(table) for w in tokens]
		# remove remaining tokens that are not alphabetic
		words = [word for word in stripped if word.isalpha()]
		# filter out stop words
		words = [w for w in words if not w in self.stop_words]
		# stamming: word root
		words = [self.stemmer.stem(w) for w in words]

		return words

	def get_freq(self,rows):
		word_freq = {}
		for row in rows:
			for w in self.clean_words(row["text"]):
				word_freq[w] = word_freq.get(w,0) + 1

		return word_freq


